<?php

namespace App\Http\Controllers;

use App\Notes;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function index()
    {
        return Notes::all();
    }

    public function store(Request $request)
    {
        $notes=$request->isMethod('put')? Notes::findorFail($request->article_id) : new Notes;
        $notes->userid=$request->input('userid');
        $notes->userprofileid=$request->input('userprofileid');
        $notes->notesid=$request->input('notesid');
        $notes->notestext=$request->input('notestext');
        
        $notes->save();
        
    }

    public function destroy($userid)
    {
        $notes=Notes::findorFail($userid);

        $notes->delete();
    }
}
