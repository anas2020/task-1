<?php

namespace App\Http\Controllers;

use App\Userprofile;
use Illuminate\Http\Request;

class UserprofileController extends Controller
{
    public function index()
    {
        return Userprofile::all();
    }
}
