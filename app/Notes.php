<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $fillable = ['userid', 'userprofileid', 'notesid','notetext','fileupload'];
        
}
