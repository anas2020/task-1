<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userprofile extends Model
{
    protected $fillable = ['Firstname', 'Lastname', 'Address'];

}
